const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const student = new Schema({
  nombre: { type: String },
  apellido: { type: String },
  correo: { type: String },
  direccion: { type: String }
  //nombre, apellido, correo electrónico, dirección
});

module.exports = mongoose.model('students', student);