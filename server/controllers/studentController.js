const Student = require("../models/studentModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const studentPost = (req, res) => {
  var student = new Student();

  student.nombre = req.body.nombre;
  student.apellido = req.body.apellido;
  student.correo = req.body.correo;
  student.direccion = req.body.direccion;

 // nombre: { type: String },
  //apellido: { type: String },
 // correo: { type: String },
  //direccion: { type: String }
  //console.log(task.nombre);
  //console.log(task.apellido);
  //console.log(task.correo);
  //console.log(task.direccion);

  if (student.nombre && student.apellido && student.correo && student.direccion) {
    student.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the task', err)
        res.json({
          error: 'There was an error saving the task'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/tasks/?id=${student.id}`
      });
      res.json(student);
    });
  } else {
    res.status(422);
    console.log('error while saving the task')
    res.json({
      error: 'No valid data provided for task'
    });
  }
};

/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const studentGet = (req, res) => {
  // if an specific task is required
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }
      res.json(student);
    });
  } else {
    // get all tasks
    Student.find(function (err, students) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(students);
    });

  }
};

/**
 * Updates a task
 *
 * @param {*} req
 * @param {*} res
 */
const studentPatch = (req, res) => {
  // get task by id
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('error while queryting the student', err)
        res.json({ error: "student doesnt exist" })
      }

      // update the student object (patch)
      student.nombre = req.body.nombre ? req.body.nombre : student.nombre;
      student.apellido = req.body.apellido ? req.body.apellido : student.apellido;
      student.correo = req.body.correo ? req.body.correo : student.correo;
      student.direccion = req.body.direccion ? req.body.direccion : student.direccion;

      //student.nombre = req.body.nombre;
      //student.apellido = req.body.apellido;
      //student.correo = req.body.correo;
      //student.direccion = req.body.direccion;

      
      // update the student object (put)
      // student.title = req.body.title
      // student.detail = req.body.detail

      student.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the student', err)
          res.json({
            error: 'There was an error saving the student'
          });
        }
        res.status(200); // OK
        res.json(student);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" })
  }
};




const studentDelete = (req, res) => {
  if (req.query && req.query.id) {
    Student.findByIdAndRemove(req.query.id, function(err, student) {
          if (err) {
              res.status(404);
              console.log('Error while queryting the Student', err)
              res.json({ error: "Student doesnt exist" })
          }
          res.json(student);
          res.status(204).send();
      });
  } 

};


module.exports = {
  studentGet,
  studentPost,
  studentPatch,
  studentDelete
}